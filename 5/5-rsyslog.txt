syslog



In this exercise, we’ll use and configure syslog

1. Use the less command to view syslog messages:
#less /var/log/kern.log

2. View the content of the correct configuration file, to see where mail logs should go:
#less /etc/rsyslog.d/50-default.conf
(search for mail)
See that this file does not exist yet.

3. Log a message yourself as if you are a mail program:
#logger -p mail.notice mail from Yuval
Now, see that /var/log/mail.log was created, and contains this messages

4. Change the destination of mail messages of notice severity to another file (e.g. <your name>.log)
Restart the rsyslogd service so that the effect takes place.
The name of the service is rsyslog.service
Now, send the log message again. See that the new file was created, and message went there.


