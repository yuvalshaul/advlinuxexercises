#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main()
{
    int f;
    char line[1000];
    ssize_t nread;

    f = open("/etc/shadow", O_RDONLY );
    if (f==-1)
    {
        perror("open:");
        exit(-1);
    }

    nread = read(f,line,200);
    if (nread > 0){
        printf("from shadow file: %s\n", line);
    }
    else{
        perror("read from shadow:");
    }

}