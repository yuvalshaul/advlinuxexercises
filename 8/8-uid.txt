
                security

1.  View the content of the password file:
        $> more /etc/passwd
    View the content of the shadow password file:
        $> more /etc/shadow
    View the content of the group
        $> more /etc/group
    View the content of the group password shadow
        $> more /etc/gshadow


2. Find what groups the user osboxes belongs to:
    Collect data from the password file, as well as the group file.
    Do the same by using the groups command.

3. Create another user (use useradd command) that is not in sudo group.

    (box2 in my example)


4. Compile and run the uidgid.c program when the current user is osboxes:

    $> make
    $> ./prog

    Notice that this program tries to print the content of /etc/shadow 
    file, so you need to run it using sudo.
    Now delete the prog file, and compile it again, this time when the
    current user is root:

    $> su - root
    $> cd (the directory where the program is)
    $> make
    $> ./prog

    This time it runs with no problems.
    Notice that the ownership of the file is root.

5. Change the uid of the prog file:

    $> chmod u+s prog
    $> ls -l prog

6. copy the file to /home/osboxes, and change the directory permissions 
    so that other user (the one you have create) could run it.
    Now, change currrent user to the other account you have created.
    Notice that this account cannot run programs with sudo.

    $> su - box2
    $> /home/osboxes/prog

    The user box2 should not be able to run any superuser programs, but it
    gets the "power" of root, because the uid bit is set on prog.

    