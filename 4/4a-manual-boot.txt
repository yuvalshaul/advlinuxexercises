Boting Linux Manually


1 - Go to GRUB command line
   During boot, when presented with the boot menu, type ‘c’ to enter the command line mode.

2 - set the root partition
   The command may depend on the partitions in your machine.
    Here’s what I used:
	set root=(sd0,msdos1)
    Try setting a wrong value (e.g: set root=(sd0,msdos2)), then try to list /boot

3 - Specify the kernel
   Use the ‘linux’ command to describe how to invoke the kernel.
   file-name
	The first parameter will be the kernel image to use.
   ro
	The ro parmeter tells the kernel to mount the root filesystem as read-only.
	 This way, file system checking will be done in quiet mode, and the kernel will later 
	re-mount the root as read-write.
   root
	Tells the kernel what partition to use as root partition (once done using the initial ram-disk)

   example:   
	linux /boot/vmlinuz-3.10.0-123.13.2.el7.x86_64   ro root=/dev/sda1


4 - ramsidk

   Tell GRUB2 to create a ram-disk in memory, using this file, and leave it for the kernel.

   example:
	initrd /boot/initramfs- 3.10.0-123.13.2.el7.x86_64.img

5 - boot !!!

   Everything should be ready.
   Boot by typing ‘boot’.



Good Luck.