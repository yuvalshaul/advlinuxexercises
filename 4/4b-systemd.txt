systemd (use systemctl)


(you may need to create a link to /usr/lib/systemd)

1. First check if systemd is installed on your system or not and what is the version of currently installed Systemd?
# systemd --version
systemd 215
+PAM +AUDIT +SELINUX +IMA +SYSVINIT +LIBCRYPTSETUP +GCRYPT +ACL +XZ -SECCOMP -APPARMOR
It’s clear from the above example, that we have systemd 215 version Installed.
2. Check where the binaries and libraries of systemd and systemctl are installed.
# whereis systemd 
systemd: /usr/lib/systemd /etc/systemd /usr/share/systemd /usr/share/man/man1/systemd.1.gz
# whereis systemctl
systemctl: /usr/bin/systemctl /usr/share/man/man1/systemctl.1.gz
3. Check whether systemd is running or not.
# ps -eaf | grep [s]ystemd
root         1     0  0 16:27 ?        00:00:00 /usr/lib/systemd/systemd --switched-root --system --deserialize 23
root       444     1  0 16:27 ?        00:00:00 /usr/lib/systemd/systemd-journald
root       469     1  0 16:27 ?        00:00:00 /usr/lib/systemd/systemd-udevd
root       555     1  0 16:27 ?        00:00:00 /usr/lib/systemd/systemd-logind
dbus       556     1  0 16:27 ?        00:00:00 /bin/dbus-daemon --system --address=systemd: --nofork --nopidfile --systemd-activation
Notice: systemd is running as parent daemon (PID=1). In the above command ps with (-e) select all Processes, (-a) select all processes except session leaders and (-f) for full format listing (i.e. -eaf).
Also note the square brackets in the above example and rest of the examples to follow. Square Bracket expression is part of grep’s character class pattern matching.
4. Analyze systemd boot process.
# systemd-analyze
Startup finished in 487ms (kernel) + 2.776s (initrd) + 20.229s (userspace) = 23.493s
5. Analyze time taken by each process at boot.
# systemd-analyze blame
8.565s mariadb.service
7.991s webmin.service
6.095s postfix.service
4.311s httpd.service
3.926s firewalld.service
3.780s kdump.service
3.238s tuned.service
1.712s network.service
1.394s lvm2-monitor.service
1.126s systemd-logind.service
....
6. Analyze critical chain at boot.
# systemd-analyze critical-chain
The time after the unit is active or started is printed after the "@" character.
The time the unit takes to start is printed after the "+" character.
multi-user.target @20.222s
└─mariadb.service @11.657s +8.565s
└─network.target @11.168s
└─network.service @9.456s +1.712s
└─NetworkManager.service @8.858s +596ms
└─firewalld.service @4.931s +3.926s
└─basic.target @4.916s
└─sockets.target @4.916s
└─dbus.socket @4.916s
└─sysinit.target @4.905s
└─systemd-update-utmp.service @4.864s +39ms
└─auditd.service @4.563s +301ms
└─systemd-tmpfiles-setup.service @4.485s +69ms
└─rhel-import-state.service @4.342s +142ms
└─local-fs.target @4.324s
└─boot.mount @4.286s +31ms
└─systemd-fsck@dev-disk-by\x2duuid-79f594ad\x2da332\x2d4730\x2dbb5f\x2d85d19608096
└─dev-disk-by\x2duuid-79f594ad\x2da332\x2d4730\x2dbb5f\x2d85d196080964.device @4
Important: Systemctl accepts services (.service), mount point (.mount), sockets (.socket) and devices (.device) as units.
7. List all the available units.
# systemctl list-unit-files
UNIT FILE                                   STATE   
proc-sys-fs-binfmt_misc.automount           static  
dev-hugepages.mount                         static  
dev-mqueue.mount                            static  
proc-sys-fs-binfmt_misc.mount               static  
sys-fs-fuse-connections.mount               static  
sys-kernel-config.mount                     static  
sys-kernel-debug.mount                      static  
tmp.mount                                   disabled
brandbot.path                               disabled
.....
8. List all running units.
# systemctl list-units
UNIT                                        LOAD   ACTIVE SUB       DESCRIPTION
proc-sys-fs-binfmt_misc.automount           loaded active waiting   Arbitrary Executable File Formats File Syste
sys-devices-pc...0-1:0:0:0-block-sr0.device loaded active plugged   VBOX_CD-ROM
sys-devices-pc...:00:03.0-net-enp0s3.device loaded active plugged   PRO/1000 MT Desktop Adapter
sys-devices-pc...00:05.0-sound-card0.device loaded active plugged   82801AA AC'97 Audio Controller
sys-devices-pc...:0:0-block-sda-sda1.device loaded active plugged   VBOX_HARDDISK
sys-devices-pc...:0:0-block-sda-sda2.device loaded active plugged   LVM PV Qzyo3l-qYaL-uRUa-Cjuk-pljo-qKtX-VgBQ8
sys-devices-pc...0-2:0:0:0-block-sda.device loaded active plugged   VBOX_HARDDISK
sys-devices-pl...erial8250-tty-ttyS0.device loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS0
sys-devices-pl...erial8250-tty-ttyS1.device loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS1
sys-devices-pl...erial8250-tty-ttyS2.device loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS2
sys-devices-pl...erial8250-tty-ttyS3.device loaded active plugged   /sys/devices/platform/serial8250/tty/ttyS3
sys-devices-virtual-block-dm\x2d0.device    loaded active plugged   /sys/devices/virtual/block/dm-0
sys-devices-virtual-block-dm\x2d1.device    loaded active plugged   /sys/devices/virtual/block/dm-1
sys-module-configfs.device                  loaded active plugged   /sys/module/configfs
...
9. List all failed units.
# systemctl --failed

UNIT          LOAD   ACTIVE SUB    DESCRIPTION
kdump.service loaded failed failed Crash recovery kernel arming
LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
1 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
10. Check if a Unit (cron.service) is enabled or not?.
# systemctl is-enabled crond.service
enabled
11. Check whether a Unit or Service is running or not?.
# systemctl status firewalld.service
firewalld.service - firewalld - dynamic firewall daemon
Loaded: loaded (/usr/lib/systemd/system/firewalld.service; enabled)
Active: active (running) since Tue 2015-04-28 16:27:55 IST; 34min ago
Main PID: 549 (firewalld)
CGroup: /system.slice/firewalld.service
└─549 /usr/bin/python -Es /usr/sbin/firewalld --nofork --nopid
Apr 28 16:27:51 tecmint systemd[1]: Starting firewalld - dynamic firewall daemon...
Apr 28 16:27:55 tecmint systemd[1]: Started firewalld - dynamic firewall daemon.
Control and Manage Services Using Systemctl
12. List all services (including enabled and disabled).
# systemctl list-unit-files --type=service
UNIT FILE                                   STATE   
arp-ethers.service                          disabled
auditd.service                              enabled 
autovt@.service                             disabled
blk-availability.service                    disabled
brandbot.service                            static  
collectd.service                            disabled
console-getty.service                       disabled
console-shell.service                       disabled
cpupower.service                            disabled
crond.service                               enabled 
dbus-org.fedoraproject.FirewallD1.service   enabled 
....
13. How do I start, restart, stop, reload and check the status of a service (httpd.service) in Linux.
# systemctl start httpd.service
# systemctl restart httpd.service
# systemctl stop httpd.service
# systemctl reload httpd.service
# systemctl status httpd.service
httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled)
Active: active (running) since Tue 2015-04-28 17:21:30 IST; 6s ago
Process: 2876 ExecStop=/bin/kill -WINCH ${MAINPID} (code=exited, status=0/SUCCESS)
Main PID: 2881 (httpd)
Status: "Processing requests..."
CGroup: /system.slice/httpd.service
├─2881 /usr/sbin/httpd -DFOREGROUND
├─2884 /usr/sbin/httpd -DFOREGROUND
├─2885 /usr/sbin/httpd -DFOREGROUND
├─2886 /usr/sbin/httpd -DFOREGROUND
├─2887 /usr/sbin/httpd -DFOREGROUND
└─2888 /usr/sbin/httpd -DFOREGROUND
Apr 28 17:21:30 tecmint systemd[1]: Starting The Apache HTTP Server...
Apr 28 17:21:30 tecmint httpd[2881]: AH00558: httpd: Could not reliably determine the server's fully q...ssage
Apr 28 17:21:30 tecmint systemd[1]: Started The Apache HTTP Server.
Hint: Some lines were ellipsized, use -l to show in full.
Note: When we use commands like start, restart, stop and reload with systemctl, we will not get any output on the terminal, only status command will print the output.
14. How to active a service and enable or disable a service at boot time (auto start service at system boot).
# systemctl is-active httpd.service
# systemctl enable httpd.service
# systemctl disable httpd.service
15. How to mask (making it impossible to start) or unmask a service (httpd.service).
# systemctl mask httpd.service
ln -s '/dev/null' '/etc/systemd/system/httpd.service'
# systemctl unmask httpd.service
rm '/etc/systemd/system/httpd.service'
16. How to a Kill a service using systemctl command.
# systemctl kill httpd
# systemctl status httpd
httpd.service - The Apache HTTP Server
Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled)
Active: failed (Result: exit-code) since Tue 2015-04-28 18:01:42 IST; 28min ago
Main PID: 2881 (code=exited, status=0/SUCCESS)
Status: "Total requests: 0; Current requests/sec: 0; Current traffic:   0 B/sec"
Apr 28 17:37:29 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:29 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:39 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:39 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:49 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:49 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:59 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 17:37:59 tecmint systemd[1]: httpd.service: Got notification message from PID 2881, but recepti...bled.
Apr 28 18:01:42 tecmint systemd[1]: httpd.service: control process exited, code=exited status=226
Apr 28 18:01:42 tecmint systemd[1]: Unit httpd.service entered failed state.
Hint: Some lines were ellipsized, use -l to show in full.
Control and Manage Mount Points using Systemctl
17. List all system mount points.
# systemctl list-unit-files --type=mount
UNIT FILE                     STATE   
dev-hugepages.mount           static  
dev-mqueue.mount              static  
proc-sys-fs-binfmt_misc.mount static  
sys-fs-fuse-connections.mount static  
sys-kernel-config.mount       static  
sys-kernel-debug.mount        static  
tmp.mount                     disabled
18. How do I mount, unmount, remount, reload system mount points and also check the status of mount points on the system.
# systemctl start tmp.mount
# systemctl stop tmp.mount
# systemctl restart tmp.mount
# systemctl reload tmp.mount
# systemctl status tmp.mount
tmp.mount - Temporary Directory
Loaded: loaded (/usr/lib/systemd/system/tmp.mount; disabled)
Active: active (mounted) since Tue 2015-04-28 17:46:06 IST; 2min 48s ago
Where: /tmp
What: tmpfs
Docs: man:hier(7)
http://www.freedesktop.org/wiki/Software/systemd/APIFileSystems
Process: 3908 ExecMount=/bin/mount tmpfs /tmp -t tmpfs -o mode=1777,strictatime (code=exited, status=0/SUCCESS)
Apr 28 17:46:06 tecmint systemd[1]: Mounting Temporary Directory...
Apr 28 17:46:06 tecmint systemd[1]: tmp.mount: Directory /tmp to mount over is not empty, mounting anyway.
Apr 28 17:46:06 tecmint systemd[1]: Mounted Temporary Directory.
19. How to active, enable or disable a mount point at boot time (auto mount at system boot).
# systemctl is-active tmp.mount
# systemctl enable tmp.mount
# systemctl disable  tmp.mount
20. How to mask (making it impossible to start) or unmask a mount points in Linux.
# systemctl mask tmp.mount
ln -s '/dev/null' '/etc/systemd/system/tmp.mount'
# systemctl unmask tmp.mount
rm '/etc/systemd/system/tmp.mount'
Control and Manage Sockets using Systemctl
21. List all available system sockets.
# systemctl list-unit-files --type=socket
UNIT FILE                    STATE   
dbus.socket                  static  
dm-event.socket              enabled 
lvm2-lvmetad.socket          enabled 
rsyncd.socket                disabled
sshd.socket                  disabled
syslog.socket                static  
systemd-initctl.socket       static  
systemd-journald.socket      static  
systemd-shutdownd.socket     static  
systemd-udevd-control.socket static  
systemd-udevd-kernel.socket  static  
11 unit files listed.
22. How do I start, restart, stop, reload and check the status of a socket (example: cups.socket) in Linux.
# systemctl start cups.socket
# systemctl restart cups.socket
# systemctl stop cups.socket
# systemctl reload cups.socket
# systemctl status cups.socket
cups.socket - CUPS Printing Service Sockets
Loaded: loaded (/usr/lib/systemd/system/cups.socket; enabled)
Active: active (listening) since Tue 2015-04-28 18:10:59 IST; 8s ago
Listen: /var/run/cups/cups.sock (Stream)
Apr 28 18:10:59 tecmint systemd[1]: Starting CUPS Printing Service Sockets.
Apr 28 18:10:59 tecmint systemd[1]: Listening on CUPS Printing Service Sockets.
23. How to active a socket and enable or disable at boot time (auto start socket at system boot).
# systemctl is-active cups.socket
# systemctl enable cups.socket
# systemctl disable cups.socket
24. How to mask (making it impossible to start) or unmask a socket (cups.socket).
# systemctl mask cups.socket
ln -s '/dev/null' '/etc/systemd/system/cups.socket'
# systemctl unmask cups.socket
rm '/etc/systemd/system/cups.socket'


Good Luck.